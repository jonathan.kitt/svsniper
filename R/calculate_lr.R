#' Calculate lr values from summary file and standardize lr values
#'
#' @param axiom_data imported Axiom files (obtained by calling read_axiom())
#' @param standard whether or not to standardize the lr values ('none', 'mean' - the default , 'ref' or 'both')
#' @param ref name of the sample to use for standardization (must be defined if method is "ref" or "both", defaults to NULL)
#'
#' @import dplyr
#'
#' @return A [tibble()].
#' @export
#'
#' @examples
#' \dontrun{
#' calculate_lr(axiom_data, standard = "mean", ref = NULL)
#' }

calculate_lr <- function(axiom_data, standard = "mean", ref = NULL) {

  # Set NULL variables

  # a <- b <- pivot_longer <- probeset_id <- NULL

  # If standard = "none", calculate lr values
  if (standard == "none") {
    message("Calculating lr values")
    lr <- axiom_data |>
      dplyr::mutate(lr = log2(sqrt(signal_a^2 + signal_b^2)),
                    .after = signal_b)
  }

  # If standard = "ref" or "both", check if ref is defined
  if(standard  %in% c("ref", "both")){
    print("Checking if reference sample is defined")
    if(is.null(ref)){
      stop("Reference sample is not defined")
    }
    if(!is.null(ref) & !ref %in% unique(axiom_data$file_name)) {
      stop("Reference sample doesn't exist in the list of samples")
    }
  }

  # If standard = "mean", calculate lr + lr_st_mean
  if (standard == "mean") {
    message("Calculating lr values and lr_st_mean values")
    lr <- axiom_data |>
      dplyr::mutate(lr = log2(sqrt(signal_a^2 + signal_b^2)),
                    .after = signal_b) |>
      dplyr::mutate(lr_mean = mean(lr, na.rm = TRUE),
                    .by = probeset_id) |>
      dplyr::mutate(lr_st_mean = lr - lr_mean,
                    .after = lr) |>
      dplyr::select(-lr_mean)
  }

  # If standard = "ref", calculatelr +  lr_st_ref
  if (standard == "ref") {
    message("Calculating lr values and lr_st_ref values")
    lr <- axiom_data |>
      dplyr::mutate(lr = log2(sqrt(signal_a^2 + signal_b^2)),
                    .after = signal_b) |>
      dplyr::mutate(lr_st_ref = lr - lr[file_name == ref],
                    .by = probeset_id,
                    .after = lr)
  }

  # If standard = "both", calculate lr + lr_st_ref & lr_st_mean
  if (standard == "both") {
    message("Calculating lr values, lr_st_mean and lr_st_ref values")
    lr <- axiom_data |>
      dplyr::mutate(lr = log2(sqrt(signal_a^2 + signal_b^2)),
                    .after = signal_b) |>
      dplyr::mutate(lr_st_ref = lr - lr[file_name == ref],
                    lr_mean = mean(lr, na.rm = TRUE),
                    .by = probeset_id) |>
      dplyr::mutate(lr_st_mean = lr - lr_mean) |>
      dplyr::select(file_name:lr, lr_st_mean, lr_st_ref, genotyping_call)
  }

  return(lr)

}
