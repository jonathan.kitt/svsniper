# Testin stuff
# 2023-05-16

# Load packages ----

library(data.table)
library(svsniper)
library(tidyverse)


# Import data ----

path <- "../../../projects/breedwheat/axiom_pipeline/tabw420k-01-01/AxiomGT1.summary.txt"

lines <- readr::read_lines(file = path, lazy = TRUE)

lines <- readr::read_lines(path, lazy = TRUE)
subset(x = lines, grepl(pattern = "^probeset_id*", lines)) |>
  stringr::str_split(pattern = "\t")


colnames <- readr::read_lines(path) |>
  subset(grepl(pattern = "^probeset_id*", .)) |>
  stringr::str_split(pattern = "\t") |>
  unlist()

lines <- vroom::vroom_lines(path)
subset(lines, grepl("^probeset_id*", lines))

dt1 <- svsniper::read_summary(path)

d1 <- vroom::vroom(path, delim = "\t", comment = "#", col_names = FALSE, )

test <- vroom::vroom_lines(file = path, comment = "#")
